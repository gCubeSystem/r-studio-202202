# Pipeline-Docker-Template

This is a template useful for building docker images and push to dockerhub.
There is a jenkinsjob template on our jenkins with the same name.


## USAGE TIPS:

* REMEMBER TO FILL the jenkinsfile environment section with your values. The following field should be properly filled: imagename, git_url
* REMEMBER TO PUT your Dockerfile in the root folder of your new project

* You can find the related jenkinsjob template here: https://jenkins.d4science.org/job/Pipeline-Docker-Template/
