#!/usr/bin/with-contenv bash
# shellcheck shell=bash

## Set our dynamic variables in Renviron.site to be reflected by RStudio Server or Shiny Server
exclude_vars="HOME PASSWORD RSTUDIO_VERSION BATCH_USER_CREATION"
for file in /var/run/s6/container_environment/*
do
  sed -i "/^${file##*/}=/d" ${R_HOME}/etc/Renviron.site
  regex="(^| )${file##*/}($| )"
  [[ ! $exclude_vars =~ $regex ]] && echo "${file##*/}=$(cat $file)" >> ${R_HOME}/etc/Renviron.site || echo "skipping $file"
done

# Download a custom Rprofile.site
wget -q https://code-repo.d4science.org/gCubeSystem/rstudio-rprofile/raw/branch/master/container-Rprofile.site -O ${R_HOME}/etc/Rprofile.site

## only file-owner (root) should read container_environment files:
chmod 600 /var/run/s6/container_environment/*
