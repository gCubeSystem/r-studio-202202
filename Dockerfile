FROM d4science/r-studio-requirement

LABEL org.d4science.image.licenses="EUPL-1.2" \
      org.d4science.image.source="https://code-repo.d4science.org/gCubeSystem/r-studio-requirement" \
      org.d4science.image.vendor="D4Science <https://www.d4science.org>" \
      org.d4science.image.authors="Andrea Dell'Amico <andrea.dellamico@isti.cnr.it>, Roberto Cirillo <roberto.cirillo@isti.cnr.it>" \
      org.d4science.image.r_version="2022.02.1+461"

ENV S6_VERSION=v2.1.0.2
ENV RSTUDIO_VERSION=2022.02.1+461
ENV DEFAULT_USER=rstudio
ENV PANDOC_VERSION=default
ENV PATH=/usr/lib/rstudio-server/bin:$PATH

RUN /rocker_scripts/install_rstudio.sh

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update --yes
RUN apt-get install -y libxt6
RUN installGithub.r -d TRUE -u FALSE -r https://packagemanager.rstudio.com/all/__linux__/focal/latest sofia-tsaf/SOFIA

# This part comes from https://github.com/openanalytics/shinyproxy-rstudio-ide-demo/blob/master/Dockerfile
RUN echo "www-frame-origin=same" >> /etc/rstudio/disable_auth_rserver.conf
RUN echo "www-verify-user-agent=0" >> /etc/rstudio/disable_auth_rserver.conf

ADD 01_set_env.sh /etc/cont-init.d/01_set_env
ADD 02_userconf.sh /etc/cont-init.d/02_userconf
ADD 03_setup_root_path.sh /etc/cont-init.d/03_setup_root_path

# By default RStudio does not give access to all enviornment variables defined in the container (e.g. using ShinyProxy).
# Uncomment the next line, to change this behavior.
#ADD 04_copy_env.sh /etc/cont-init.d/04_copy_env
ADD 05_setup_rsession_parameters.sh /etc/cont-init.d/05_setup_rsession_parameters

# Prepare the workspace environment
RUN mkdir -p /opt/workspace-lib
RUN mkdir /var/log/workspace-lib
ADD https://maven.d4science.org/nexus/content/repositories/gcube-snapshots/org/gcube/data-access/sh-fuse-integration/2.0.0-SNAPSHOT/sh-fuse-integration-2.0.0-20211005.090627-1-jar-with-dependencies.jar /opt/workspace-lib/fuse-workspace.jar
ADD 06_workspace_mount.sh /etc/cont-init.d/06_workspace_mount
ADD 06-fuse-logback.xml /opt/workspace-lib/logback.xml

EXPOSE 8787

CMD ["/init"]
